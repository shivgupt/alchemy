#!/usr/bin/env bash

####################
# ENV VARS

DOMAINNAME=$DOMAINNAME; [[ -n "$DOMAINNAME" ]] || DOMAINANME=localhost
EMAIL=$EMAIL; [[ -n "$EMAIL" ]] || EMAIL=noreply@gmail.com
MODE=$MODE; [[ -n "$MODE" ]] || MODE=dev
ETH_PROVIDER="http://eth.bohendo.com:8545"
ETH_NETWORK_ID="1"
ETH_MNEMONIC="candy maple cake sugar pudding cream honey rich smooth crumble sweet treat"

####################

if [[ "$MODE" == "live" ]]
then version="`cat package.json | jq .version | tr -d '"'`"
else version="latest"
fi

registry=registry.gitlab.com
user="`whoami`"
project=alchemy

database_image=postgres:10
ipfs_image=ipfs/go-ipfs:latest
if [[ "$DOMAINNAME" == "localhost" ]]
then
  proxy_image=${project}_proxy:latest
  cacher_image=${project}_cacher:latest
  server_image=${project}_server:latest
  graph_image=${project}_graph:latest
else
  proxy_image=$registry/$user/$project/proxy:$version
  cacher_image=$registry/$user/$project/cacher:$version
  server_image=$registry/$user/$project/server:$version
  graph_image=$registry/$user/$project/graph:$version
fi

function pull_if_unavailable {
    if [[ -z "`docker image ls | grep ${1%:*} | grep ${1#*:}`" ]]
    then
        docker pull $1
    fi
}

pull_if_unavailable $proxy_image
pull_if_unavailable $cacher_image
pull_if_unavailable $server_image
pull_if_unavailable $graph_image
pull_if_unavailable $database_image
pull_if_unavailable $ipfs_image

function new_secret {
    secret=$2
    if [[ -z "$secret" ]]
    then
        secret=`head -c 32 /dev/urandom | xxd -plain -c 32 | tr -d '\n\r'`
    fi
    if [[ -z "`docker secret ls -f name=$1 | grep -w $1`" ]]
    then
        id=`echo $secret | tr -d '\n\r' | docker secret create $1 -`
        echo "Created secret called $1 with id $id"
    fi
}

new_secret database_graph
new_secret database_server

mkdir -p /tmp/$project
cat - > /tmp/$project/docker-compose.yml <<EOF
version: '3.4'

secrets:
  database_server:
    external: true
  database_graph:
    external: true

volumes:
  cache:
  graph:
  ipfs:
  letsencrypt:
  server:

services:
  proxy:
    image: $proxy_image
    environment:
      - DOMAINNAME=$DOMAINNAME
      - EMAIL=$EMAIL
    ports:
      - "80:80"
      - "443:443"
    volumes:
      - letsencrypt:/etc/letsencrypt

  cacher:
    image: $cacher_image
    environment:
      NODE_ENV: production
      API_URL: http://server:3001
      ETH_MNEMONIC: $ETH_MNEMONIC
      ETH_NETWORK_ID: $ETH_NETWORK_ID
      ETH_PROVIDER: $ETH_PROVIDER
      REDIS_URL: redis://localhost:6379
      arcjs_network: live
      arcjs_providerPort: 8545
      arcjs_providerUrl: http://eth.bohendo.com
    volumes:
      - cache:/app/cache

  server:
    image: $server_image
    environment:
      NODE_ENV: production
      DB_HOST: server_db
      DB_PORT: 5432
      DB_NAME: $project
      DB_USERNAME: $project
      DB_PASSWORD_FILE: /run/secrets/database_server
    secrets:
      - database_server

  server_db:
    image: $database_image
    environment:
      POSTGRES_USER: $project
      POSTGRES_DB: $project
      POSTGRES_PASSWORD_FILE: /run/secrets/database_server
    secrets:
      - database_server
    volumes:
      - server:/var/lib/postgresql/data

  graph:
    image: $graph_image
    environment:
      network_id: $ETH_NETWORK_ID
      postgres_host: graph_db:5432
      postgres_user: $project
      postgres_db: $project
      postgres_pass_file: /run/secrets/database_graph
      ipfs: ipfs:5001
      ethereum: live:$ETH_PROVIDER
    secrets:
      - database_graph

  graph_db:
    image: $database_image
    environment:
      POSTGRES_USER: $project
      POSTGRES_DB: $project
      POSTGRES_PASSWORD_FILE: /run/secrets/database_graph
    secrets:
      - database_graph
    volumes:
      - graph:/var/lib/postgresql/data

  ipfs:
    image: $ipfs_image
    volumes:
      - ipfs:/data/ipfs
EOF

docker stack deploy -c /tmp/$project/docker-compose.yml $project
rm -rf /tmp/$project

echo -n "Waiting for the $project stack to wake up."
while true
do
    num_awake="`docker container ls | grep $project | wc -l | sed 's/ //g'`"
    sleep 3
    if [[ "$num_awake" == "7" ]]
    then break
    else echo -n "."
    fi
done
echo " Good Morning!"
sleep 2
