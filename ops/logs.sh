#!/bin/bash

name=$1
shift
set -e
docker service ps --no-trunc alchemy_$name
sleep 1
docker service logs --tail 100 --follow alchemy_$name $@
