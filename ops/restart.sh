#!/bin/bash

ops="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
flag=$1; [[ -n "$flag" ]] || flag=dev

if [[ "$flag" == "prod" || "$flag" == "dev" ]]
then
  bash "$ops/stop.sh"
  make "$flag"
  bash "$ops/deploy.$flag.sh"

else
  docker service scale alchemy_$flag=0
  if [[ -z "`docker service ls | grep alchemy_ethprovider`" ]]
  then
    make prod
  else
    make dev
  fi
  docker service scale alchemy_$flag=1
fi
