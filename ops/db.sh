#!/usr/bin/env bash

project=alchemy

if [[ "$1" == "server" ]]
then
  service=${project}_server_db
else
  service=${project}_graph_db
fi

container=`for f in $(docker service ps -q $service)
do
  docker inspect --format '{{.Status.ContainerStatus.ContainerID}}' $f
done | head -n1`

if [[ -z "$2" ]]
then
    docker exec -it $container bash -c "psql $project --username=$project"
else
    docker exec -it $container bash -c "psql $project --username=$project --command=\"$2\""
fi
