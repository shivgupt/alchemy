
daemon off;
user nginx;
pid /run/nginx.pid;
worker_processes 1;

events {
    worker_connections 1024;
}

http {
    sendfile on;
    tcp_nopush on;
    tcp_nodelay on;
    keepalive_timeout 65;
    types_hash_max_size 2048;
    include /etc/nginx/mime.types;
    default_type application/octet-stream;

    access_log /var/log/nginx/access.log;
    error_log  /var/log/nginx/error.log;

    gzip on;
    gzip_disable "msie6";

    ####################
    # HTTP server configuration
    server {
        listen 80;
        server_name $hostname;

        root /var/www/html/;
        index index.html;
        try_files $uri $uri/ $uri/index.html =404;

        location /explorer { }

        location / {
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_pass http://client:3000/;
        }

        location /api/graph/ {
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_pass http://graph:8000/cli/graphql/;
        }

        location /api/cache/ {
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_pass http://cacher:3002/;
        }

        location /api/data/ {
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_pass http://server:3001/;
        }

        location /ipfs/ {
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_pass http://ipfs:8080/ipfs/;
        }

    }

    ####################
    # HTTPS server configuration
    server {
        listen 443 ssl;
        server_name $hostname;
        ssl_dhparam         /etc/ssl/dhparam.pem;
        ssl_certificate     /etc/certs/fullchain.pem;
        ssl_certificate_key /etc/certs/privkey.pem;
        ssl_session_cache shared:le_nginx_SSL:1m;
        ssl_session_timeout 1440m;
        ssl_protocols TLSv1.1 TLSv1.2;
        ssl_prefer_server_ciphers on;
        ssl_ciphers 'EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH';
        ssl_ecdh_curve secp384r1;

        root /var/www/html/;
        index index.html;
        try_files $uri $uri/ $uri/index.html =404;

        location /explorer { }

        location / {
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_pass http://client:3000/;
        }

        location /api/graph/ {
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_pass http://graph:8000/cli/graphql/;
        }

        location /api/cache/ {
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_pass http://cacher:3002/;
        }

        location /api/data/ {
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_pass http://server:3001/;
        }

        location /ipfs/ {
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_pass http://ipfs:8080/ipfs/;
        }

    }
}
