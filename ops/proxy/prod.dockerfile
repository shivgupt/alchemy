FROM alpine:3.8

RUN apk add --update --no-cache bash certbot iputils nginx openssl && \
    openssl dhparam -out /etc/ssl/dhparam.pem 2048 && \
    ln -fs /dev/stdout /var/log/nginx/access.log && \
    ln -fs /dev/stderr /var/log/nginx/error.log

COPY ops/proxy/prod.conf /etc/nginx/nginx.conf
COPY ops/proxy/entry.sh /root/entry.sh
COPY modules/client/build/public /var/www/html
COPY modules/client/build/public/assets/favicons/favicon.ico /var/www/html/favicon.ico
COPY modules/explorer/build /var/www/html/explorer/
COPY modules/graphiql /var/www/html/graphiql

ENTRYPOINT ["bash", "/root/entry.sh"]
