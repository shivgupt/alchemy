# Decentralize Proposal Metadata

## Tokens Requested
 - 8 ETH - Pay for food & shelter during an expected 40 hours of work (+/- 20)
 - 0.5% Reputation - to keep up w reputation inflation

### About me

https://bohendo.com/hello-daostack/

## TL;DR

Proposal data (title, url) is currently being stored on a private server. If something goes wrong, this data could be lost forever. If someone gains control of this private server, they could sabotage votes by replacing proposal data with something malicious. Both of these problems could be solved by storing proposal data on IPFS.

## The Problem

"One simple litmus test for whether or not a blockchain project is truly decentralized: can a third party independently make a client for it, and do everything that the "official" client can?" - [Vitalik Non-giver of Ether](https://twitter.com/vitalikbuterin/status/1020661669079519233?lang=en)

I love DAOstack. As a protocol, it's amazingly decentralized. But Alchemy, the app built on top of DAOstack, is not decentralized.. Yet. I could create another copy of Alchemy but it will not have access to proposal data (title & url) as it's not stored in a decentralized way.

Currently, when you submit a proposal, the title + url is hashed & that hash is submitted to the blockchain while the actual data is stored privately.

This gives us a single point of failure: If the data on this server is lost then we lose all the privately held proposal data. The hash on the blockchain would help us identify that some title+url is correct but it can't help us get that info if we don't have it already.

The proposal title & description are extremely sensitive information. If what's shown to users is different than what's fingerprinted by the hash on the blockchain, then members will be voting against missing or fraudulent data.

An attacker would be in a position where if they can hack this one computer containing the proposal data, then they can mislead everyone. Or destroy the data and we would have no way to recover.

Who's the sysadmin guarding this data? Idk, it's not public knowledge. But the public must trust that this sysadmin is benevolent and also that s/he retains control of their servers.

## The Solution

We put proposal data on IPFS.

Any client will then be able to fetch the data on it's own without needing to trust anything other than the source code running the IPFS server, ethereum node, and itself.

The DevOps Robustification Project part 2 will include deploying an IPFS node along side the old alchemy servers which is an important step.

Now, all that's left to do is teach the client how to put it's data onto IPFS and how to retrieve it.

On [line 689 of Alchemy's arcActions file](https://github.com/daostack/alchemy/blob/dev/src/actions/arcActions.ts#L689), there's an instruction to hash the proposal description and then store that has on the blockchain. All we need to do is add this description to IPFS which will return an IPFS hash, and then store this hash on the blockchain instead. Nothing needs to change with the contracts, the client just needs to use the existing contracts a bit differently.

Up above in this same file, there's a function that retrieves proposal details. This too will need to be tweaked to retrieve from IPFS rather than the private server.

## The Implications

This proposal may go against the DAOstack core team's wishes.

They have a task on their Trello board to add authentication to their private data server preventing any other app from accessing it. If this task is completed, it would kill the DAO explorer and any other 3rd party app that would like to access proposal data.

But they also have a task to host their data on IPFS so I'm not exactly sure whether they're planning to keep things decentralized or are planning to create a walled garden..

Other random clients knowing that the stored hash points to an ipfs object is powerful. Deciding on something they might be able to expect is another thing. If we start using IPFS then the contents of the proposal are publicly known & immutable.

We can start attaching important stuff more directly to the blockchain.

 - A Peepeth-sized TL;DR? It would be awesome if we could help newcomers get a feel for what kind of proposals are important to a DAO by including 140-char summaries as part of the top-level overview, available at a glance.

 - Whisper/status chat-room ID? Use this to for proposal-specific real-time communication? Alchemy could even display an interface to this chat room in a way that looks like comments.

 - We could attach a document (eg objectives, a mission statement, a legal document) and this document will be completely immutable (unless another proposal passes to update something) and be preserved for all time on IPFS. This effectively implements 

## Timeline

TBD

## Future iterations

Start using the data in this IPFS document to upgrade the capabilities of these DAOs eg by linking Status chatrooms 

