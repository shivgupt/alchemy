# get absolute paths to important dirs
cwd=$(shell pwd)
client=$(cwd)/modules/client
server=$(cwd)/modules/server
contracts=$(cwd)/modules/contracts
graph=$(cwd)/modules/graph
explorer=$(cwd)/modules/explorer

# Use the version number from package.json to tag prod docker images
version=$(shell cat package.json | grep "\"version\":" | grep -o "[0-9.]\+")
net_version=4447

# Special variable: tells make here we should look for prerequisites
VPATH=ops:build:$(client)/build:$(client)/build/public:$(contracts)/build:$(server)/build:$(graph)/build

find_options=-type f -not -path "*/node_modules/*" -not -name "*.swp"
client_src=$(shell find $(client)/src $(client)/ops $(find_options))
contract_src=$(shell find $(contracts)/contracts $(contracts)/migrations $(contracts)/ops $(find_options))
explorer_src=$(shell find $(explorer)/src $(explorer)/ops $(explorer)/public $(find_options))
graph_src=$(shell find $(graph)/src $(graph)/ops $(find_options))
proxy_src=$(shell find ops/proxy $(find_options))
server_src=$(shell find $(server)/src $(server)/ops $(find_options))

app=alchemy
cacher_image=$(app)_cacher
client_image=$(app)_client
server_image=$(app)_server
ethprovider_image=$(app)_ethprovider
graph_image=$(app)_graph
proxy_image=$(app)_proxy

# Make sure these directories exist
$(shell mkdir -p build $(client)/build $(server)/build $(contracts)/build $(explorer)/build $(graph)/build)
me=$(shell whoami)

webpack=./node_modules/.bin/webpack
graph_cli=./node_modules/.bin/graph
truffle=./node_modules/.bin/truffle

builder_options=--name=buidler --tty --rm
	
####################
# Begin phony rules
.PHONY: default all dev prod clean

default: subgraph-dev subgraph-prod
all: dev prod
dev: cacher-dev-image client-dev-image server-image proxy-dev-image ethprovider-dev-image graph-dev-image
prod: cacher-prod-image server-image proxy-prod-image graph-prod-image
clean:
	rm -rf build
	rm -rf $(client)/build
	rm -rf $(server)/build
	rm -rf $(explorer)/build
	rm -rf $(graph)/build

purge: clean
	bash ops/stop.sh
	rm -rf $(contracts)/build
	docker container stop ipfs 2> /dev/null || true
	docker container prune --force
	docker volume rm `docker volume ls -q | grep "[0-9a-f]\{64\}" | tr '\n' ' '` 2> /dev/null || true
	docker volume rm --force alchemy_cache_dev 2> /dev/null
	docker volume rm --force alchemy_chain_dev 2> /dev/null
	docker volume rm --force alchemy_graph_dev 2> /dev/null
	docker volume rm --force alchemy_ipfs_dev 2> /dev/null
	docker volume rm --force alchemy_server_dev 2> /dev/null

tags: prod
	docker tag $(proxy_image):latest registry.gitlab.com/$(me)/alchemy/proxy:$(version)
	docker tag $(cacher_image):latest registry.gitlab.com/$(me)/alchemy/cacher:$(version)
	docker tag $(server_image):latest registry.gitlab.com/$(me)/alchemy/server:$(version)
	docker tag $(graph_image):latest registry.gitlab.com/$(me)/alchemy/graph:$(version)

deploy: tags
	docker push registry.gitlab.com/$(me)/alchemy/proxy:$(version)
	docker push registry.gitlab.com/$(me)/alchemy/server:$(version)
	docker push registry.gitlab.com/$(me)/alchemy/cacher:$(version)
	docker push registry.gitlab.com/$(me)/alchemy/graph:$(version)

####################
# Begin Real Rules

# Proxy

proxy-prod-image: $(proxy_src) explorer.js client.prod.js
	docker build --file ops/proxy/prod.dockerfile --tag $(proxy_image):latest .
	@touch build/proxy-prod-image

proxy-dev-image: $(proxy_src) explorer.js
	docker build --file ops/proxy/dev.dockerfile --tag $(proxy_image):dev .
	@touch build/proxy-dev-image

# Alchemy client

client-dev-image: client.dev.js
	docker build --file $(client)/ops/client/dev.dockerfile --tag $(client_image):dev $(client)
	@touch build/client-dev-image

client.prod.js: client-builder $(client_src)
	docker run $(builder_options) \
	  --volume=$(client)/ops:/app/ops \
	  --volume=$(client)/src:/app/src \
	  --volume=$(client)/build:/app/build \
	  --volume=$(client)/tsconfig.json:/app/tsconfig.json \
	  client_builder:dev --config ops/client/webpack.prod.js

client.dev.js: client-builder $(client_src)
	docker run $(builder_options) \
	  --volume=$(client)/ops:/app/ops \
	  --volume=$(client)/src:/app/src \
	  --volume=$(client)/build:/app/build \
	  --volume=$(client)/tsconfig.json:/app/tsconfig.json \
	  client_builder:dev --config ops/client/webpack.dev.js

client-builder: $(client)/package.json ops/builder.dockerfile
	docker build --file ops/builder.dockerfile --tag client_builder:dev $(client)
	@touch build/client-builder

# Cacher server

cacher-prod-image: cacher.prod.js $(client)/ops/cacher/prod.dockerfile $(client)/ops/cacher/entry.prod.sh
	docker build --file $(client)/ops/cacher/prod.dockerfile --tag $(cacher_image):latest $(client)
	@touch build/cacher-prod-image

cacher-dev-image: cacher.dev.js $(client)/ops/cacher/dev.dockerfile $(client)/ops/cacher/entry.dev.sh
	docker build --file $(client)/ops/cacher/dev.dockerfile --tag $(cacher_image):dev $(client)
	@touch build/cacher-dev-image

cacher.prod.js: client-builder $(client_src) $(client)/ops/cacher/webpack.prod.js
	docker run $(builder_options) \
	  --volume=$(client)/ops:/app/ops \
	  --volume=$(client)/src:/app/src \
	  --volume=$(client)/build:/app/build \
	  --volume=$(client)/tsconfig.json:/app/tsconfig.json \
	  client_builder:dev --config ops/cacher/webpack.prod.js

cacher.dev.js: client-builder $(client_src) $(client)/ops/cacher/webpack.dev.js
	docker run $(builder_options) \
	  --volume=$(client)/ops:/app/ops \
	  --volume=$(client)/src:/app/src \
	  --volume=$(client)/build:/app/build \
	  --volume=$(client)/tsconfig.json:/app/tsconfig.json \
	  client_builder:dev --config ops/cacher/webpack.dev.js

# Server for proposal-info

server-image: server-builder $(server_src) $(server)/ops/Dockerfile $(server)/ops/entry.sh
	docker build --file $(server)/ops/Dockerfile --tag $(server_image):dev --tag $(server_image):latest $(server)
	@touch build/server-image

server-builder: $(server)/package.json ops/builder.dockerfile
	docker build --file ops/builder.dockerfile --tag server_builder:dev $(server)
	@touch build/server-builder

# Explorer client

explorer.js: explorer-builder $(explorer_src) $(explorer)/ops/dockerfile $(explorer)/ops/webpack.config.dev.js
	docker run $(builder_options) \
	  --volume=$(explorer)/src:/app/src \
	  --volume=$(explorer)/ops:/app/ops \
	  --volume=$(explorer)/public:/app/public \
	  --volume=$(explorer)/build:/app/build \
	  --entrypoint=yarn explorer_builder:dev build
	@touch build/explorer.js

explorer-builder: ops/builder.dockerfile $(explorer)/package.json
	docker build --file ops/builder.dockerfile --tag explorer_builder:dev $(explorer)
	@touch build/explorer-builder

# Graph node & subgraph

graph-prod-image: subgraph-prod $(graph)/ops/build.sh $(graph)/ops/entry.sh
	docker build --file $(graph)/ops/graph.dockerfile --tag $(graph_image):latest $(graph)
	@touch build/graph-prod-image

graph-dev-image: subgraph-dev $(graph)/ops/build.sh $(graph)/ops/entry.sh
	docker build --file $(graph)/ops/graph.dockerfile --tag $(graph_image):dev $(graph)
	@touch build/graph-dev-image

subgraph-prod: contract-artifacts graph-builder $(graph_src)
	docker network create ipfs 2> /dev/null || true
	docker run --name ipfs --network=ipfs --rm --detach --publish=5001:5001 ipfs/go-ipfs 2> /dev/null || true
	docker run $(builder_options) \
	  --volume=$(contracts)/build:/contracts/build \
	  --volume=$(graph)/src:/app/src \
	  --volume=$(graph)/ops:/app/ops \
	  --volume=$(graph)/build:/app/build \
	  --volume=$(graph)/tsconfig.json:/app/tsconfig.json \
	  --network=ipfs --entrypoint=bash \
    graph_builder:dev ops/build.sh 1
	@touch build/subgraph-prod

subgraph-dev: contract-artifacts graph-builder $(graph_src)
	docker network create ipfs 2> /dev/null || true
	docker run --name ipfs --network=ipfs --rm --detach --publish=5001:5001 ipfs/go-ipfs 2> /dev/null || true
	docker run $(builder_options) \
	  --volume=$(contracts)/build:/contracts/build \
	  --volume=$(graph)/src:/app/src \
	  --volume=$(graph)/ops:/app/ops \
	  --volume=$(graph)/build:/app/build \
	  --volume=$(graph)/tsconfig.json:/app/tsconfig.json \
	  --network=ipfs --entrypoint=bash \
    graph_builder:dev ops/build.sh 4447
	@touch build/subgraph-dev

graph-builder: $(graph)/package.json ops/builder.dockerfile
	docker build --file ops/builder.dockerfile --tag graph_builder:dev $(graph)
	@touch build/graph-builder

# Etherum provider: ganache + truffle 

ethprovider-dev-image: contract-artifacts $(contract_ops)
	docker build --file $(contracts)/ops/dev.dockerfile --tag $(ethprovider_image):dev $(contracts)
	@touch build/ethprovider-dev-image

contract-artifacts: contract-builder $(contract_src)
	docker run $(builder_options) \
	  --volume=$(contracts)/ops:/app/ops \
	  --volume=$(contracts)/build:/app/build \
	  --volume=$(contracts)/contracts:/app/contracts \
	  --volume=$(contracts)/truffle.js:/app/truffle.js \
	  --entrypoint=bash contract_builder:dev ops/build.sh
	@touch build/contract-artifacts

contract-builder: ops/builder.dockerfile $(contracts)/package.json
	docker build --file ops/builder.dockerfile --tag contract_builder:dev $(contracts)
	@touch build/contract-builder
