//this migration file is used only for testing purpose
var AbsoluteVote = artifacts.require('./AbsoluteVote.sol');
var Avatar = artifacts.require('./Avatar.sol');
var ContributionReward = artifacts.require('./ContributionReward.sol');
var ControllerCreator = artifacts.require('./ControllerCreator.sol');
var DaoCreator = artifacts.require('./DaoCreator.sol');
var DaoToken = artifacts.require('./DaoToken.sol');
var GenesisProtocol = artifacts.require('./GenesisProtocol.sol');
var GlobalConstraintRegistrar = artifacts.require('./GlobalConstraintRegistrar.sol');
var QuorumVote = artifacts.require('./QuorumVote.sol');
var SchemeRegistrar = artifacts.require('./SchemeRegistrar.sol');
var SimpleICO = artifacts.require('./SimpleICO.sol');
var TokenCapGC = artifacts.require('./TokenCapGC.sol');
var UpgradeScheme = artifacts.require('./UpgradeScheme.sol');
var VestingScheme = artifacts.require('./VestingScheme.sol');
var VoteInOrganizationScheme = artifacts.require('./VoteInOrganizationScheme.sol');
var OrganizationRegister = artifacts.require('./OrganizationRegister.sol');
var Redeemer = artifacts.require('./Redeemer.sol');
var UController = artifacts.require('./UController.sol');

// Stupid truffle using stupid old version of web3
const toWei = web3.utils ? web3.utils.toWei : web3.toWei;
const options = { gas: 6100000 };

// DAO parameters:
const orgName = "Genesis Alpha";
const tokenName = "Genesis Alpha";
const tokenSymbol = "GDT";
const nFounders = 5;
const founders = []; // will be filled in during migration
const initRep = Array(nFounders).fill(toWei("1000"))
const initToken = Array(nFounders).fill(toWei("1000"))
const cap = toWei("100000000","ether");
const orgNativeTokenFee = 0;

const genesisProtocolParams = {
  preBoostedVoteRequiredPercentage: 50,
  preBoostedVotePeriodLimit: 1814400,
  boostedVotePeriodLimit: 259200,
  thresholdConstA: toWei(7),
  thresholdConstB: 3,
  minimumStakingFee: toWei(0),
  quietEndingPeriod: 86400,
  proposingRepRewardConstA: 5,
  proposingRepRewardConstB: 5,
  stakerFeeRatioForVoters: 50,
  votersReputationLossRatio: 1,
  votersGainRepRatioFromLostRep: 80,
  daoBountyConst: 75,
  daoBountyLimit: toWei(100),
}

// Permission bits:
//  00001 is registered
//  00010 can register scheme
//  00100 can remove global contraint
//  01000 can upgrade controller
//  10000 can call delegate call
const Permissions = {
  schemeRegistrar: parseInt('11111' ,2),
  constraintRegistrar: parseInt('00101', 2),
  upgradeScheme: parseInt('01011', 2),
  contributionReward: parseInt('00001', 2),
  genesisProtocol: parseInt('00001', 2),
}

module.exports = function (deployer, network, accounts) {
  return (deployer.then(async function() {

    // construct array of founders
    for (let i=0; i<5; i++) { founders[i] = accounts[i]; }

    // Get instances of universal contracts
    const uControllerInst = await UController.deployed()
    const daoCreatorInst = await DaoCreator.deployed();

    const genesisProtocolInst = await GenesisProtocol.deployed();
    const schemeRegistrarInst = await SchemeRegistrar.deployed();
    const upgradeSchemeInst = await UpgradeScheme.deployed();
    const gcRegistrarInst = await GlobalConstraintRegistrar.deployed();
    const contributionRewardInst = await ContributionReward.deployed();

    const returnedParams = await daoCreatorInst.forgeOrg(
      orgName,
      tokenName,
      tokenSymbol,
      founders,
      initToken,
      initRep,
      uControllerInst.address,
      cap,
      options
    );

    const avatarInst = await Avatar.at(returnedParams.logs[0].args._avatar);
    const reputationAddr = await avatarInst.nativeReputation()
    const tokenAddr = await avatarInst.nativeToken()
    console.log(`  Avatar: ${avatarInst.address}`)
    console.log(`  NativeReputation: ${reputationAddr}`)
    console.log(`  NativeToken: ${tokenAddr}`)

    await genesisProtocolInst.setParameters([
      genesisProtocolParams.preBoostedVoteRequiredPercentage,
      genesisProtocolParams.preBoostedVotePeriodLimit,
      genesisProtocolParams.boostedVotePeriodLimit,
      genesisProtocolParams.thresholdConstA,
      genesisProtocolParams.thresholdConstB,
      genesisProtocolParams.minimumStakingFee,
      genesisProtocolParams.quietEndingPeriod,
      genesisProtocolParams.proposingRepRewardConstA,
      genesisProtocolParams.proposingRepRewardConstB,
      genesisProtocolParams.stakerFeeRatioForVoters,
      genesisProtocolParams.votersReputationLossRatio,
      genesisProtocolParams.votersGainRepRatioFromLostRep,
      genesisProtocolParams.daoBountyConst,
      genesisProtocolParams.daoBountyLimit,
    ]);
    const genesisProtocolParamsHash = await genesisProtocolInst.getParametersHash([
      genesisProtocolParams.preBoostedVoteRequiredPercentage,
      genesisProtocolParams.preBoostedVotePeriodLimit,
      genesisProtocolParams.boostedVotePeriodLimit,
      genesisProtocolParams.thresholdConstA,
      genesisProtocolParams.thresholdConstB,
      genesisProtocolParams.minimumStakingFee,
      genesisProtocolParams.quietEndingPeriod,
      genesisProtocolParams.proposingRepRewardConstA,
      genesisProtocolParams.proposingRepRewardConstB,
      genesisProtocolParams.stakerFeeRatioForVoters,
      genesisProtocolParams.votersReputationLossRatio,
      genesisProtocolParams.votersGainRepRatioFromLostRep,
      genesisProtocolParams.daoBountyConst,
      genesisProtocolParams.daoBountyLimit,
    ]);

    await schemeRegistrarInst.setParameters(
      genesisProtocolParamsHash,
      genesisProtocolParamsHash,
      genesisProtocolInst.address
    );
    const schemeRegisterParamsHash = await schemeRegistrarInst.getParametersHash(
      genesisProtocolParamsHash,
      genesisProtocolParamsHash,
      genesisProtocolInst.address
    );

    await gcRegistrarInst.setParameters(
      genesisProtocolParamsHash,
      genesisProtocolInst.address
    );
    const gcRegistrarParamsHash = await gcRegistrarInst.getParametersHash(
      genesisProtocolParamsHash,
      genesisProtocolInst.address
    );

    await upgradeSchemeInst.setParameters(
      genesisProtocolParamsHash,
      genesisProtocolInst.address
    );
    const upgradeSchemeParmsHash = await upgradeSchemeInst.getParametersHash(
      genesisProtocolParamsHash,
      genesisProtocolInst.address
    );

    await contributionRewardInst.setParameters(
      orgNativeTokenFee,
      genesisProtocolParamsHash,
      genesisProtocolInst.address
    );
    const contributionRewardParmsHash = await contributionRewardInst.getParametersHash(
      orgNativeTokenFee,
      genesisProtocolParamsHash,
      genesisProtocolInst.address
    );

    return daoCreatorInst.setSchemes(
      avatarInst.address,
      [
        schemeRegistrarInst.address,
        gcRegistrarInst.address,
        upgradeSchemeInst.address,
        contributionRewardInst.address,
        genesisProtocolInst.address,
      ], [
        schemeRegisterParamsHash,
        gcRegistrarParamsHash,
        upgradeSchemeParmsHash,
        contributionRewardParmsHash,
        genesisProtocolParamsHash,
      ], [
        Permissions.schemeRegistrar,
        Permissions.constraintRegistrar,
        Permissions.upgradeScheme,
        Permissions.contributionReward,
        Permissions.genesisProtocol,
      ]
    );

  }));
};
