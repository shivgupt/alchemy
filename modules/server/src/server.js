'use strict';

const bodyParser = require('body-parser');
const boot = require('loopback-boot');
const fs = require('fs');
const loopback = require('loopback');

const host = process.env.DB_HOST;
const port = process.env.DB_PORT;
const database = process.env.DB_NAME;
const user = process.env.DB_USERNAME;
const password = fs.readFileSync(process.env.DB_PASSWORD_FILE, 'utf8');

const options = {
  config: require('./config.json'),
  models: require('./models.json'),
  components: require('./components.json'),
  middleware: require('./middleware.json'),
  dataSources: {
    "postgresql": {
      "connector": "postgresql",
      "url": `postgres://${user}:${password}@${host}:${port}/${database}`,
      host,
      port,
      database,
      password,
      user,
    }
  }
}

if (process.env.NODE_ENV !== 'development') {
  options.components = { "loopback-component-explorer": null }
  options.middleware['final:after'] = {
    "strong-error-handler": {}
  }
}

var app = module.exports = loopback();

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use((req, res, next) => {
  console.log(`=> ${req.method}: ${req.path} -- ${JSON.stringify(req.body, null, 2)}`)
  console.log(`   `)
  next();
});

app.start = function() {
  // start the web server
  return app.listen(function() {
    app.emit('started');
    var baseUrl = app.get('url').replace(/\/$/, '');
    console.log('Alchemy data server listening at: %s', baseUrl);
    if (app.get('loopback-component-explorer')) {
      var explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
    }
  });
};

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, options, function(err) {
  if (err) throw err;

  // start the server if `$ node server.js`
  if (require.main === module)
    app.start();
});
