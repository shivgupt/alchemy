#!/bin/bash

function wait_for {
  target=server_db; echo -n "Waiting for $target to wake up."
  while true; do
    ping -c1 -w1 $target > /dev/null 2> /dev/null
    if [[ "$?" == "0" ]]; then
      echo " Good morning!"
      break
    fi
    echo -n "."
    sleep 3
  done
}

wait_for server_db

PGPASSWORD=`cat $DB_PASSWORD_FILE` psql \
  --username=$DB_USERNAME \
  --host=$DB_HOST \
  --port=$DB_PORT \
  $DB_NAME 2> psql_output <<EOF
\d+
EOF

if [[ -n "`cat psql_output | grep \"Did not find any relations.\"`" ]]
then
  echo 'Database empty, initializing tables'
  node src/create-lb-tables.js
  node src/migrate.js
else
  echo 'Tables already created, started server'
fi

if [[ "$NODE_ENV" == "production" ]]
then
  exec node src/server.js
else
  exec nodemon --legacy-watch --exitcrash --watch src/server.js src/server.js
fi
