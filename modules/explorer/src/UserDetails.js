import React from 'react';
import { StakeTable } from './StakeTable'
import { VoteTable } from './VoteTable'
import { Proposal } from './Proposal'

class UserDetails extends React.Component {
    filterProposal(proposal) {
        const { viewProposal, alchemyData } = this.props
        if (alchemyData[proposal.proposalId])
            return true
        else
            return false
    }
    render() {
        const { user, viewProposal, alchemyData } = this.props
        //console.log(JSON.stringify(user, null, 2))
        return (
            <div>
                <h2> User: {user.address} </h2> 
                <p>Proposed </p>
                {user.proposals.map( proposal => this.filterProposal(proposal) ?
                    <Proposal proposal={proposal} alchemyData={alchemyData} viewProposal={viewProposal} />
                    : null
                )}
                <p>Staked On</p>
                {user.stakes.map( stake => this.filterProposal(stake.proposal) ?
                    <StakeTable stake={stake} viewProposal={viewProposal} alchemyData={alchemyData} />
                    : null
                )}

                <p>Voted On</p>
                {user.votes.map( vote => this.filterProposal(vote.proposal) ?
                    <VoteTable vote={vote} viewProposal={viewProposal} alchemyData={alchemyData} />
                    : null
                )}
            </div>
        )
    }
}

export { UserDetails }
