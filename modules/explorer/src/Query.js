let stakes = `stakes {
    stakerAddress
    stakeAmount
    prediction
}`

let votes = `votes {
    voterAddress
    voteOption
}`

let rewards = `rewards {
    rewardId{
        type
        amount
    }
    accountId
    proposalId
}`

let common = `
    proposalId
    proposer
    daoAvatarAddress
    state
    decision
`
const q = {}

q.getAllProposals = () => {
    return `
        query {
            proposals { 
                id
                decision
            }
        }
    `
}

q.getAllProposalsWithDetails = () => {
    return `
        query {
            proposals(
                where:{
                    proposalId_not: ""
                }
            ) {
                ${common}
                ${stakes}
                ${votes}
                ${rewards}
            }
        }
    `
}

q.getAllProposalsFor = (avatar) => {
    return `
        query {
            proposals (
                where: {
                    proposalId_not: "",
                    daoAvatarAddress: "${avatar}"
                }
            ) {
                proposalId
                decision
            }
        }
    `
}

q.getAllProposalsWithDetailsFor = (avatar) => {
    return `
        query {
            proposals (
                where: {
                    proposalId_not: "",
                    daoAvatarAddress: "${avatar}"
                }
            ) {
                ${common}
                ${stakes}
                ${votes}
                ${rewards}
            }
        }
    `
}

q.getAllProposalsProposedBy = (proposer) => {
    return `
        query {
            proposals (
                where: {
                    proposalId_not: "",
                    proposer: "${proposer}"
                }
            ) {
                ${common}
                ${stakes}
                ${votes}
                ${rewards}
            }
        }
    `
}

q.getAllProposalsVotedBy = (voter) => {
    return `
        query {
            votes(
                where: {
                    voterAddress: "${voter}"
                }
            ){
                voteOption
                proposal {
                    ${common}
                }
            }
        }
    `
}

q.getAllProposalsStakedBy = (staker) => {
    return `
        query {
            stakes(
                where: {
                    stakerAddress: "${staker}"
                }
            ){
                vote
                stake
                proposal {
                    ${common}
                }
            }
        }
    `
}

q.getDetailsOfProposal = (id) => {
    return `
        query {
            proposal(id: "${id}") {
                ${common}
                ${stakes}
                ${votes}
                ${rewards}
            }
        }
    `
}

/*
q.getAllRewardsFor = (beneficiary) => {
    return `
        query {
            rewards (
                where: {
                    beneficiary_in: ["${beneficiary}"]
                }
            ) {
                ${rewards}
            }
        }
    `
}
*/

q.getAllUsers = (avatar) => {
    return `
        query {
            accounts (
                where: {
                    daoAvatarAddress: "${avatar}"
                }
            ){address}
        }
    `
}

q.getAllDetailsForUser = (user, avatar) => {
    return `
        query {
            accounts (
                where: {
                    address: "${user}"
                    daoAvatarAddress: "${avatar}"
                }
            ) {
                accountId
                address
                proposals {
                    proposalId
                    decision
                }
                stakes {
                    prediction
                    stakeAmount
                    proposal {proposalId}
                }
                votes {
                    voteOption
                    proposal {proposalId}
                }
            }
        }
    `
}

export default q
