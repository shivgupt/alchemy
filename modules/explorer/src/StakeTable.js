import React from 'react';

class StakeTable extends React.Component {
    render() {
        const { stake, viewProposal, alchemyData } = this.props
        if (!stake) return null
        return (
            <div>
                    <div key={stake.proposal.proposalId} className="row">
                        <div className="col-7">
                            <a href={alchemyData[stake.proposalId.proposalId].description}>{alchemyData[stake.proposalId.proposalId].title}</a>
                        </div>
                        <div className="col-3">
                            {stake.stakeAmount}
                        </div>
                        <div className="col-1">
                            {stake.prediction === "1" ? <p> For </p> : <p> Against </p>}
                        </div>
                        <div className="col-1">
                            <button onClick={() => viewProposal(stake.proposalId.proposalId)}>
                                Info
                            </button>
                        </div>
                    </div>
            </div>
        )
    }
}

export { StakeTable }
