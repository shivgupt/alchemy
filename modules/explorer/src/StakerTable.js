import React from 'react';

class StakerTable extends React.Component {
    render() {
        const { stake, viewUser, alchemyData } = this.props
        if (!stake) return null
        return (
            <div>
                    <div key={stake.stakerAddress} className="row">
                        <div className="col-7">
                            {stake.stakerAddress}
                        </div>
                        <div className="col-3">
                            {stake.stakeAmount}
                        </div>
                        <div className="col-1">
                            {stake.prediction === "1" ? <p> For </p> : <p> Against </p>}
                        </div>
                        <div className="col-1">
                            <button onClick={() => viewUser(stake.stakerAddress)}>
                                Info
                            </button>
                        </div>
                    </div>
            </div>
        )
    }
}

export { StakerTable }
